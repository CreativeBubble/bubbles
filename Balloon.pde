class Balloon {

  // ATTRIBUTES
  float x, y;
  float speed;
  float velo_x = random(-2, 2);
  float velo_y = random(-2, 2);
  int r = 255;
  int g = 255;
  int b = 255;
  color c = color(r, g, b);
  float max_mass = 50*50;
  float size = random(10, 30);
  float mass = size*size;
  float randomNoisePosition = random(100);  // each balloon will move according to different noise values.
  // prevents that all balloons follow the same path
  boolean colordown = true;

  // CONSTRUCTOR - it is named exactly like the classname
  // it is used to construct / initialize one object of the class
  // we can pass arguments to the constructor, which is especially useful if we want
  // to initialize different objects of the class with different values
  // (different colors, different starting positions)

  Balloon(float _x, float _y, float _speed, color _c) {
    x = _x;
    y =_y;
    speed = _speed;
    c = _c;
  }

  Balloon(float _x, float _y, float _speed) {
    x = _x;
    y =_y;
    speed = _speed;
  }

  //GetterSetter

  float get_x() {
    return x;
  }
  float get_y() {
    return y;
  }
  float get_size() {
    return size;
  }
  float get_v_x() {
    return velo_x;
  }
  float get_v_y() {
    return velo_y;
  }
  void set_x(float new_x) {
    x = new_x;
    return;
  }
  void set_y(float new_y) {
    y = new_y;
    return;
  }
  void set_v_x(float new_v_x) {
    velo_x = new_v_x;
    return;
  }
  void set_v_y(float new_v_y) {
    velo_y = new_v_y;
    return;
  }




  // FUNCTIONS
  void update(int i) {
    //println("update int "+i);
    move();
    //applyWindRandom(5);
    applyWindNoise(1, 1, frameRate/100.0);
    borderCondition();
    pyramidcrash(i);


    if (intersect_ball(i) == true) {
      crash(i);
    }
    drawSelf();
    check_speed();
  }

  void pyramidcrash(int i){
    for (int z= 0; z<myPyramids.size(); z++)
    {
      Pyramid_S3 actualpy = myPyramids.get(z);
      Homo_point_2d p_p_a = new Homo_point_2d(actualpy.a.x, actualpy.a.y);
      Homo_point_2d p_p_b = new Homo_point_2d(actualpy.b.x, actualpy.b.y);
      Homo_point_2d p_p_c = new Homo_point_2d(actualpy.c.x, actualpy.c.y);

      Homo_line_2d l1 = get_homoline_by_crossproduct(p_p_a, p_p_b);
      Homo_line_2d l2 = get_homoline_by_crossproduct(p_p_b, p_p_c);
      Homo_line_2d l3 = get_homoline_by_crossproduct(p_p_a, p_p_c);
      //    println("homolinescreatet  ");
      //      println("l11: " + l1.a + "   l1b: " + l1.b + "   l1c: " + l1.c);
      Homo_point_2d p_b = new Homo_point_2d(x, y);

      //check crash with line a
      float distance1 = ((l1.a*p_b.u+l1.b*p_b.v+l1.c*p_b.w));
      float small_x = g_b_s(actualpy.a.x, actualpy.b.x);
      float small_y = g_b_s(actualpy.a.y, actualpy.b.y);
      float high_x = g_b_h(actualpy.a.x, actualpy.b.x);
      float high_y = g_b_h(actualpy.a.y, actualpy.b.y);
      if (distance1<500 && distance1 > -500 && small_x <= x && high_x >= x && small_y <= y && high_y >= y) {
        println("Distance of ball" +i +" is: " + distance1 + " to line 1 of Pyramid: " + z);
        crash_pyramid(l1, p_b);
      }

      //check crash with line b
      float distance2 = ((l2.a*p_b.u+l2.b*p_b.v+l2.c*p_b.w));
      small_x = g_b_s(actualpy.c.x, actualpy.b.x);
      small_y = g_b_s(actualpy.c.y, actualpy.b.y);
      high_x = g_b_h(actualpy.c.x, actualpy.b.x);
      high_y = g_b_h(actualpy.c.y, actualpy.b.y);
      if (distance2<500 && distance2 > -500 && small_x <= x && high_x >= x && small_y <= y && high_y >= y) {
        println("Distance of ball" +i +" is: " + distance2 + " to line 2 of Pyramid: " + z);
        crash_pyramid(l2, p_b);
        }

      // check crash with line c
      float distance3 = ((l3.a*p_b.u+l3.b*p_b.v+l3.c*p_b.w));
      small_x = g_b_s(actualpy.c.x, actualpy.a.x);
      small_y = g_b_s(actualpy.c.y, actualpy.a.y);
      high_x = g_b_h(actualpy.c.x, actualpy.a.x);
      high_y = g_b_h(actualpy.c.y, actualpy.a.y);
      if (distance3<500 && distance3 > -500 && small_x <= x && high_x >= x && small_y <= y && high_y >= y) {
        println("Distance of ball" +i +" is: " + distance3 + " to line 3 of Pyramid: " + z);
        crash_pyramid(l3, p_b);
      }
    }
  }

  void crash_pyramid(Homo_line_2d crashed_line, Homo_point_2d crashing_point){
    Homo_point_2d velo_p = new Homo_point_2d(velo_x, velo_y);
    PVector velo_vec = new PVector(velo_p.u, velo_p.v, velo_p.w);
    float magnitude = velo_vec.mag();
    println(magnitude);
    velo_vec.normalize();
    PVector crashed_line_vec = new PVector(crashed_line.a, crashed_line.b, crashed_line.c);
    float a = PVector.angleBetween(velo_vec, crashed_line_vec);
    println(degrees(a));
    float angle_incoming = degrees(a);
    float angle_outcomming = (180 - angle_incoming*2);
//angle_outcomming = (sqrt(angle_outcomming*angle_outcomming));
    println(angle_outcomming);
    PVector v = PVector.fromAngle(angle_outcomming);
    println(v);
    v.mult(magnitude);
    println("new velo vec:  "+ v);
    //Homo_line_2d ballline = get_homoline_by_crossproduct(crashing_point, velo_p);
  //  Homo_point_2d crashing_linepoint = get_intersectionpoint(crashed_line, ballline);

    velo_x = v.x;
    velo_y = v.y;
  }

  //give back smaller float
  float g_b_s(float a, float b){
    if(a<=b){
      return a;
    }
    else{return b;}

  }
  //give back higher float
  float g_b_h(float a, float b){
    if(b<=a){
      return a;
    }
    else{
    return b;
  }
  }

  Homo_point_2d get_intersectionpoint(Homo_line_2d x, Homo_line_2d y){
    float z1 = (x.b*y.c-x.c*y.b);
    float z2 = (x.c*y.a-x.a*y.c);
    float z3 = (x.a*y.b-x.b*y.a);
    Homo_point_2d h_p = new Homo_point_2d(z1, z2, z3);
    return h_p;
  }

  Homo_line_2d get_homoline_by_crossproduct(Homo_point_2d x, Homo_point_2d y) {
    float z1 = (x.v*y.w-x.w*y.v);
    float z2 = (x.w*y.u-x.u*y.w);
    float z3 = (x.u*y.v-x.v*y.u);
    Homo_line_2d h_l = new Homo_line_2d(z1, z2, z3);
    return h_l;
  }

  Homo_line_2d create_Homo_line_2d(float _x1, float _y1, float _x2, float _y2) {
    float x1 = _x1;
    float y1 = _y1;
    float x2 = _x2;
    float y2 = _y2;
    float m = compute_m(x1, y1, x2, y2);
    float n = compute_n(x1, y1, m);
    float y0 = compute_y0(x1, m, n);
    float x0 = compute_x0(y1, m, n);
    Homo_line_2d output = get_homo_line_by_2_2dpoints(x0, y0);
    return output;
  }

  void check_speed() {
    if (velo_x>2||velo_x<-2) {
      velo_x = velo_x*0.9;
    }
    if (velo_y>2||velo_y<-2) {
      velo_y = velo_y*0.9;
    }
  }

  boolean intersect_ball(int own_number) {
    //  println("intersect_with_otherball check with: ");

    for (int i = 0; i<myBalloons.size(); i++) {
      if (i != own_number) {
        //print(i+ " ");
        float anotherballlon_x = myBalloons.get(i).get_x();
        float anotherballlon_y = myBalloons.get(i).get_y();
        float anotherballon_size = myBalloons.get(i).get_size();

        if (dist(x, y, anotherballlon_x, anotherballlon_y)<(size/2+anotherballon_size/2)) {
          return true;
        }
      }
    }
    return false;
  }
  /*
  void crash( ) {
   velo_y = velo_y*-(noise(velo_y)*2);
   velo_x = velo_x*-(noise(velo_x)*2);
   }
   */
  void crash(int i) {
    //velo_y = (velo_y*-1);
    //  velo_x = (velo_x*-1);

    float velo_y_crashpartner = myBalloons.get(i).get_v_y();
    float velo_x_crashpartner = myBalloons.get(i).get_v_x();

    float y_crashpartner = myBalloons.get(i).get_y();
    float x_crashpartner = myBalloons.get(i).get_x();

    velo_y_crashpartner = velo_y_crashpartner*-1+noise(frameCount)/5;
    velo_x_crashpartner = velo_x_crashpartner*-1-noise(frameCount)/5;


    if (velo_y<0) {
      y= y-3;
    }
    if (velo_y>0) {
      y= y+3;
    }
    if (velo_x<0) {
      x= x-3;
    }
    if (velo_x>0) {
      x= x+3;
    }
    if (velo_y_crashpartner<0) {
      y_crashpartner = y_crashpartner-3;
    }
    if (velo_y_crashpartner>0) {
      y_crashpartner = y_crashpartner+3;
    }
    if (velo_x_crashpartner<0) {
      x_crashpartner = x_crashpartner-3;
    }
    if (velo_x_crashpartner>0) {
      x_crashpartner = x_crashpartner+3;
    }

    myBalloons.get(i).set_y(y_crashpartner);
    myBalloons.get(i).set_x(x_crashpartner);

    myBalloons.get(i).set_v_y(velo_y_crashpartner);
    myBalloons.get(i).set_v_x(velo_x_crashpartner);

    if (colordown == true ) {
      r = r - 5;
      g = g - 8;
      b = b - 4;
      //fill(r,g,b);
    }
    if (r < 0 && g < 0 && b < 0) {
      colordown = false;
    }
    if (colordown == false) {
      r = r+5;
      g = g+8;
      b = b+4;
      //fill(r,g,b);
    }

    if (r>255 && g>255|| b>255 && g>255 || r>255 && b>255) {
      colordown = true;
    }
  }

  void drawSelf() {
    noStroke();
    fill(r, g, b);
    ellipse(x, y, size, size);
  }

  void move() {
    x = x + velo_x;
    //print("X " + x);
    y = y + velo_y;
    //println("y" +y);
  }

  void applyWindRandom(float strength) {
    x += random(-strength, strength);
  }


  void applyWindNoise(float smoothness, float strength, float windChange) {
    // calculate wind based upon balloon position
    // add windchange, to receive slightly different noise values in each frame → more dynamic result
    // add randomNoisePosition, to receive different noise values for each balloon → they do not follow exactly the same path
    float nX = x/smoothness + windChange + randomNoisePosition;
    float nY = y/smoothness + windChange + randomNoisePosition;
    float wind = (noise(nX, nY)-0.5) * strength;

    x += wind;
  }

  void borderCondition() {
    if (y>=height-size/2||y<0+size/2) {
      velo_y = velo_y*-1;
    }
    if (y<0+size/2) {
      y = y+2;
    }
    if (y>=height-size/2) {
      y = y-2;
    }
    if (x>=width-size/2||x<0+size/2) {
      velo_x = velo_x*-1;
    }
    if (x>=width-size/2) {
      x = x-2;
    }
    if (x<0+size/2) {
      x = x+2;
    }
  }

  // this function is used to determine if a balloon was hit with the right mouse button
  boolean isHit() {
    float distanceClickBalloon = dist(x, y, mouseX, mouseY);
    if (distanceClickBalloon<size/2 && mousePressed & mouseButton == RIGHT) {
      return true;
    } else {
      return false;
    }
  }
}
