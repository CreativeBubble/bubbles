// CLASSES - OBJECT ORIENTATED PROGRAMMING 03

// now instead of an usual array, we store our objects in an Arraylist

ArrayList<Pyramid_S3> myPyramids = new ArrayList<Pyramid_S3>();

ArrayList<Balloon> myBalloons = new ArrayList<Balloon>();
PShape texture;


void setup () {
  println("setup");
  size(600, 600, P3D);
  smooth(4);
  texture = loadShape("jingjang_ohneg.svg");

  // initialize the balloon array - see the function at the bottom of the code
  initPyramid();
  initBalloons(1);
}

void draw () {
  //  println("draw");
  background(50);

//  draw_pyramid();
  draw_pyramid2();


  shape(texture, 200, 200);
  //adress all balloons in a for loop and update them
  for (int i = 0; i<myBalloons.size(); i++)
  {
    // instead of myBalloons[i], ArrayList elements are adressed with myBalloons.get(i)
    //println("baloon" + i);
    myBalloons.get(i).update(i);
    //delete balloons with the mouse (right click)
    if (myBalloons.get(i).isHit()) {
      myBalloons.remove(i);
    }
  }
}

void initPyramid() {
  Point p1 = new Point(110, 100, 0);
  Point p2 = new Point(105, 201, 0);
  Point p3 = new Point(200, 200, 0);
  Point p4 = new Point(120, 150, 20);
  Point p5 = new Point(300, 200, 0);
  Point p6 = new Point(250, 400, 0);
  Point p7 = new Point(500, 420, 0);
  Point p8 = new Point(400, 380, 20);

  Pyramid_S3 pyr = new Pyramid_S3(p1, p2, p3, p4);
  myPyramids.add(pyr);
  Pyramid_S3 pyr2 = new Pyramid_S3(p5, p6, p7, p8);
  myPyramids.add(pyr2);
}

void draw_pyramid() {
  pushMatrix();
  noFill();
  stroke(10);
  stroke(255, 0, 0);
  beginShape();
  vertex(100, 100, 0);
  vertex(200, 200, 0);
  vertex(10, 200, 0);
  vertex(100, 100, 0);

  endShape();
  popMatrix();
}

void draw_pyramid2() {
  for (int i = 0; i < myPyramids.size(); i++) {
    myPyramids.get(i).drawSelf();
  }
}

void mousePressed() {

  if (mouseButton == LEFT) {
    // create a new balloon upon mouseclick
    myBalloons.add(new Balloon(mouseX, mouseY, 2, color(random(255), random(255), random(255))));
  }
}


void initBalloons(int num) {
  for (int i = 0; i<num; i++) {
    //    Balloon new_one = new Balloon(random(width), random(height), 2, color(random(255), random(255), random(255)));
    Balloon new_one = new Balloon(random(width), random(height), 2);
    if (new_one.intersect_ball(myBalloons.size()-1) == false) {
      //myBalloons.add(new_one);
      myBalloons.add(new_one);
      //myBalloons.add(new Balloon(random(width), random(height), 2, color(random(255), random(255), random(255))));
    }
  }
}
